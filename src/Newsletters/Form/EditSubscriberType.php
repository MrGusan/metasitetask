<?php

namespace App\Newsletters\Form;

use App\Newsletters\Entity\Subscriber;
use App\Newsletters\Entity;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Count;
use Symfony\Component\Validator\Constraints\Email;

class EditSubscriberType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class,
                [
                    'constraints'  => [new NotBlank(['groups' => 'form_validation_only'])],
                ])
            ->add('email', EmailType::class,
                [
                    'constraints'  => [new NotBlank(['groups' => 'form_validation_only']), new Email(['groups' => 'form_validation_only'])],
                ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class' => Subscriber::class,
                'validation_groups' => ['form_validation_only']
            ]
        );
    }
}
