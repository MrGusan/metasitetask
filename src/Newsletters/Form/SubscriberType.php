<?php

namespace App\Newsletters\Form;

use App\Newsletters\Entity\Subscriber;
use App\Newsletters\Entity;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Count;
use Symfony\Component\Validator\Constraints\Email;
use App\Newsletters\Validator\Constraints\UniqueSubscriberEmail;

class SubscriberType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class,
                [
                    'constraints'  => [new NotBlank(['groups' => 'form_validation_only'])],
                ])
            ->add('email', EmailType::class,
                [
                    'constraints'  => [new NotBlank(['groups' => 'form_validation_only']), new Email(['groups' => 'form_validation_only']), new UniqueSubscriberEmail(['groups' => 'form_validation_only'])],
                ])
            ->add('categories', ChoiceType::class,
                [
                    'choices'         => $options['categories']->toArray(),
                    'choice_label'    => 'title',
                    'choice_value'    => 'id',
                    'multiple'        => true,
                    'constraints'  => [new Count(['min' => 1, 'minMessage' => 'At least one category must be selected', 'groups' => 'form_validation_only'])],
                ])
            ->add('submit', SubmitType::class,
                [
                    'label' => 'Subscribe'
                ]);

        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) {

            /**@var Entity\Subscriber $subscriber */
                $subscriber = $event->getData();
                if(!empty($subscriber->getId())){
                    $categoriesArray = $subscriber->getCategories();

                    $categories = [];

                    foreach($categoriesArray as $categoryArray) {
                        $category = new Entity\Category();
                        $category->setId($categoryArray['id']);
                        $category->setTitle($categoryArray['title']);

                        $categories[] = $category;
                    }

                    $subscriber->setCategories($categories);
                }

        });
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class' => Subscriber::class,
                'categories' => null,
                'validation_groups' => ['form_validation_only']
            ]
        );
    }
}
