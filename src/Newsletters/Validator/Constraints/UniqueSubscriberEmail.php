<?php
namespace App\Newsletters\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class UniqueSubscriberEmail extends Constraint
{
    public $message = 'This email address has already subscribed.';

    public function validatedBy()
    {
        return \get_class($this).'Validator';
    }

    public function getTargets(){
        return 'class';
    }
}