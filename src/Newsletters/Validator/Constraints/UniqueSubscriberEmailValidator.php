<?php

namespace App\Newsletters\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use App\Newsletters\Entity;
use App\Newsletters\Services;

class UniqueSubscriberEmailValidator extends ConstraintValidator
{
    /** @var Services\Subscriber */
    private $subscriberService;

    public function __construct(Services\Subscriber $subscriberService)
    {
        $this->subscriberService = $subscriberService;
    }

    public function validate($email, Constraint $constraint)
    {
        if(empty($email)) {
            return;
        }

        if (!empty($this->subscriberService->findByEmail($email))) {
            $this->context->buildViolation($constraint->message)->addViolation();
        }
    }
}