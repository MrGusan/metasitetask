<?php

namespace App\Newsletters\Entity;

use Symfony\Component\Security\Core\User\UserInterface;


class User implements UserInterface
{
    private $username;
    private $password;
    private $roles = [];

    public function getPassword()
    {
        return $this->password;
    }

    public function setPassword($pass)
    {
         $this->password = $pass;

        return $this;
    }

    public function eraseCredentials()
    {
        // TODO: Implement eraseCredentials() method.
    }

    public function getUsername()
    {
        return $this->username;
    }

    public function setUsername($username)
    {
        $this->username = $username;

        return $this;
    }

    public function getRoles()
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function getSalt()
    {
        // TODO: Implement getSalt() method.
    }
}