<?php

namespace App\Newsletters\Entity;


class Subscriber
{
    private $id;
    private $createdAt;
    private $name;
    private $email;
    private $categories = [];

    public function getId(): ?string
    {
        return $this->id;
    }

    public function setId(string $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getCreatedAt(): int
    {
        return $this->createdAt;
    }

    public function setCreatedAt(int $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
       $this->name = $name;

       return $this;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getCategories()
    {
       return $this->categories;
    }

    public function setCategories(array $categories): self
    {
       $this->categories = $categories;

       return $this;
    }
}