<?php

namespace App\Newsletters\Services;

use App\Newsletters\Entity;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ArrayDenormalizer;
use Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer;
use Symfony\Component\Serializer\Serializer;

class User
{
    /** @var string */
    private $userPath;

    /**
     * @param string $userPath
     */
    public function __construct(string $userPath)
    {
        $this->userPath = $userPath;
    }

    public function findByUsername(string $username)
    {
        $users = $this->loadAll();

        if (!empty($users)) {
            return $users->filter(
                function ($user) use ($username) {
                    /** @var Entity\User $user */
                    return $user->getUsername() == $username;
                })->current();
        }

        return null;
    }

    public function loadAll()
    {
        $users = null;

        $fileSystem = new Filesystem();

        if ($fileSystem->exists($this->userPath)) {
            $loadedData = $this->loadFromFile();
            $users = !empty($loadedData) ? new ArrayCollection($this->loadFromFile()) : new ArrayCollection();
        }

        return $users;
    }

    public function loadFromFile()
    {
        $content = file_get_contents($this->userPath);
        if (!empty($content)) {
            return $this->deserialize($content);
        }

        return null;
    }

    private function deserialize($content)
    {
        $encoders = [new JsonEncoder()];
        $normalizers = [new GetSetMethodNormalizer(), new ArrayDenormalizer()];
        $serializer = new Serializer($normalizers, $encoders);

        return $serializer->deserialize($content, 'App\Newsletters\Entity\User[]', 'json');
    }
}
