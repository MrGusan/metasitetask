<?php

namespace App\Newsletters\Services;

use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ArrayDenormalizer;
use Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer;
use Symfony\Component\Serializer\Serializer;

class Category
{

    /** @var string  */
    private $categoriesPath;

    /**
     * @param string $categoriesPath
     */
    public function __construct(string $categoriesPath)
    {
        $this->categoriesPath = $categoriesPath;
    }

    public function loadAll()
    {
        $subscribers = null;

        $fileSystem = new Filesystem();

        if($fileSystem->exists($this->categoriesPath)){
            $loadedData = $this->loadFromFile();
            $subscribers = !empty($loadedData) ? new ArrayCollection($this->loadFromFile()) : new ArrayCollection();
        }

        return $subscribers;
    }

    public function loadFromFile()
    {
        $content = file_get_contents($this->categoriesPath);
        if(!empty($content)) {
            return $this->deserialize($content);
        }

        return null;
    }

    private function deserialize($content)
    {
        $encoders = [new JsonEncoder()];
        $normalizers = [new GetSetMethodNormalizer(), new ArrayDenormalizer()];
        $serializer = new Serializer($normalizers, $encoders);

        return $serializer->deserialize($content, 'App\Newsletters\Entity\Category[]', 'json');
    }
}
