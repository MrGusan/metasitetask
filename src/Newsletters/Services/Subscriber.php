<?php

namespace App\Newsletters\Services;

use App\Newsletters\Entity;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer;
use Symfony\Component\Serializer\Normalizer\ArrayDenormalizer;
use Symfony\Component\Serializer\Serializer;
use Doctrine\Common\Collections\ArrayCollection;

class Subscriber
{

    /** @var string  */
    private $subscriberPath;

    /**
     * @param string $subscriberPath
     */
    public function __construct(string $subscriberPath)
    {
        $this->subscriberPath = $subscriberPath;
    }

    public function getNew()
    {
        return new Entity\Subscriber();
    }

    public function loadAll()
    {
        $subscribers = null;

        $fileSystem = new Filesystem();

        if($fileSystem->exists($this->subscriberPath)){
            $loadedData = $this->loadFromFile();
            $subscribers = !empty($loadedData) ? new ArrayCollection($this->loadFromFile()) : new ArrayCollection();
        }

        return $subscribers;
    }

    public function findByEmail(string $email)
    {
        $subscribers = $this->loadAll();

        if(!empty($subscribers)) {
            return $subscribers->filter(
                function ($subscriber) use ($email) {
                    /** @var Entity\Subscriber $subscriber */
                    return $subscriber->getEmail() == $email;
                })->current();
        }

        return null;
    }

    public function find(string $id)
    {
        $subscribers = $this->loadAll();

        if(!empty($subscribers)) {
            return $subscribers->filter(
                function ($subscriber) use ($id) {
                    /** @var Entity\Subscriber $subscriber */
                    return $subscriber->getId() == $id;
                })->current();
        }

        return null;
    }

    public function update(Entity\Subscriber $subscriber)
    {
        $key = null;
        $subscribers = $this->loadAll();

        if(!empty($subscribers)) {
            $key = $subscribers->indexOf($subscribers->filter(
                function ($savedSubscriber) use ($subscriber) {
                    /** @var Entity\Subscriber $savedSubscriber */
                    return $savedSubscriber->getId() == $subscriber->getId();
                })->current());
        }

        if($key !== null) {
            $subscribers->set($key, $subscriber);
            $this->saveToFile($subscribers);
        }
    }

    public function save(Entity\Subscriber $subscriber)
    {
        $subscribers = $this->loadAll();

        $subscribers->add($subscriber);

        $this->saveToFile($subscribers);
    }

    public function remove(string $id)
    {
        $subscriber = null;

        $subscribers = $this->loadAll();

        if(!empty($subscribers)) {
            $subscriber = $subscribers->filter(
                function ($subscriber) use ($id) {
                    /** @var Entity\Subscriber $subscriber */
                    return $subscriber->getId() == $id;
                })->current();
        }

        if(!empty($subscriber)) {
            $subscribers->removeElement($subscriber);
            $this->saveToFile($subscribers);
        }
    }

    public function saveToFile(ArrayCollection $subscribers)
    {
        file_put_contents($this->subscriberPath,  $this->serialize($subscribers->toArray()));
    }

    public function loadFromFile()
    {
        $content = file_get_contents($this->subscriberPath);
        if(!empty($content)) {
            return $this->deserialize($content);
        }

        return null;
    }

    private function serialize($content)
    {
        $encoders = [new JsonEncoder()];
        $normalizers = [new GetSetMethodNormalizer()];
        $serializer = new Serializer($normalizers, $encoders);

        return $serializer->serialize($content, 'json');
    }

    private function deserialize($content)
    {
        $encoders = [new JsonEncoder()];
        $normalizers = [new GetSetMethodNormalizer(), new ArrayDenormalizer()];
        $serializer = new Serializer($normalizers, $encoders);

        return $serializer->deserialize($content, 'App\Newsletters\Entity\Subscriber[]', 'json');
    }
}
