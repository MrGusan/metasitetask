<?php

namespace App\Newsletters\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Newsletters\Services;
use App\Newsletters\Form\SubscriberType;
use App\Newsletters\Form\EditSubscriberType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class SubscriberController extends AbstractController
{
    /**@var Services\Subscriber */
    private $subscriberService;

    /**@var Services\Category */
    private $categoryService;

    public function __construct(
        Services\Subscriber $subscriberService,
        Services\Category $categoryService
    )
    {
        $this->subscriberService    = $subscriberService;
        $this->categoryService      = $categoryService;
    }

    /**
     * @Route("/", name="subscriber.list")
     */
    public function list()
    {
        $subscribers = $this->subscriberService->loadAll();

        return $this->render('subscriber/list.twig', ['subscribers' => $subscribers]);
    }

    /**
     * @param Request $request
     *
     * @Route("/subscribe", name="subscriber.subscribe")
     *
     * @return Response
     */
    public function subscribe(Request $request)
    {
        $subscriber =  $this->subscriberService->getNew();
        $form = $this->createForm(SubscriberType::class,
            $subscriber, ['categories' => $this->categoryService->loadAll()]
        );

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            $subscriber->setCreatedAt(time());
            $subscriber->setId(uniqid());

            $this->subscriberService->save($subscriber);

            return $this->render('subscriber/successfully_subscribed.twig');
        }

        return $this->render('subscriber/subscribe_form.twig', [
            'form'    => $form->createView()
        ]);
    }

    /**
     * @param Request $request
     * @param string $id
     *
         * @Route("/subscriber/{id}/edit", name="subscriber.edit", requirements={"id"="\w+"})
     *
     * @return Response
     */
    public function edit(Request $request, string $id)
    {
        $subscriber =  $this->subscriberService->find($id);

        $form = $this->createForm(
            EditSubscriberType::class,
            $subscriber,
            [
                'action' => $this->generateUrl('subscriber.edit', ['id' => $subscriber->getId()]),
            ]
        );

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            $this->subscriberService->update($subscriber);

            return new Response("Successfully updated!", 202);
        }

        return $this->render('subscriber/edit_form.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @param string $id
     *
     * @Route("/subscribe/{id}/remove", name="subscriber.remove", requirements={"id"="\w+"})
     *
     * @return Response
     */
    public function remove(string $id)
    {
        $this->subscriberService->remove($id);
        return new Response("Successfully removed!", 202);
    }
}