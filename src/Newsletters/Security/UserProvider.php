<?php

namespace App\Newsletters\Security;

use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use App\Newsletters\Entity\User;
use App\Newsletters\Services;

class UserProvider implements UserProviderInterface
{

    /** @var Services\User */
    private $userService;

    /**
     * @param Services\User $userService
     */
    public function __construct(Services\User $userService)
    {
        $this->userService = $userService;
    }

    public function loadUserByUsername($username)
    {
        $user = $this->userService->findByUsername($username);

        if(!empty($user)){
            return $user;
        }

        throw new UsernameNotFoundException('No user was found');
    }

    /**
     * @return UserInterface
     */
    public function refreshUser(UserInterface $user)
    {
        if (!$user instanceof User) {
            throw new UnsupportedUserException(sprintf('Invalid user class "%s".', get_class($user)));
        }

        return $user;
    }

    public function supportsClass($class)
    {
        return User::class === $class;
    }
}