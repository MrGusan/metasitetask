import './style.css';

const $ = require('jquery');
require('bootstrap');
require('datatables.net-bs4')();

class SubscriberList {

    constructor() {

        this.editModal                  = $("#edit-subscriber__modal");

        this.editButtonSelector         = ".edit-subscriber__button";
        this.editSubmitButtonSelector   = ".edit-subscriber__button--submit";
        this.deleteButtonSelector       = ".delete-subscriber__button";
    }

    bindEvents() {
        const body = $("body");
        const self = this;

        body.on("click", self.editButtonSelector, function () {
            self.openEditForm($(this).data("url"));
        });

        body.on("click", self.editSubmitButtonSelector, function () {
            self.submitEditForm($(this).closest("form"));
        });

        body.on("click", self.deleteButtonSelector, function () {
            self.deleteAction($(this).data("url"));
        });
    }

    deleteAction(url){
        $.ajax({
            type: 'POST',
            url: url,
            success: function (data, status, xhr) {
                window.location = window.location;
            }
        });
    }

    openEditForm(url) {
        const self = this;

        $.ajax({
            type: 'POST',
            url: url,
            success: function (data, status, xhr) {
                self.editModal.find(".modal-body").html(data);
                self.editModal.modal("show");
            }
        });
    }

    submitEditForm(form) {
        const self = this;
        let formData = new FormData(form[0]);

        $.ajax({
            type: 'POST',
            url: form.attr("action"),
            data: formData,
            processData: false,
            contentType: false,
            success: function (data, status, xhr) {
                if (xhr.status === 202) {
                    self.editModal.modal("hide");
                    window.location = window.location;
                }
                if (xhr.status === 200) {
                    self.editModal.find(".modal-body").html(data);
                }
            }
        });
    }
}

(function () {

    $('[data-toggle="popover"]').popover();
    $("#subscribers-list").DataTable( {
        "columnDefs": [ {
            "targets": 'no-sort',
            "orderable": false,
        }]});

    let subscriberList = new SubscriberList();

    subscriberList.bindEvents();
})();

